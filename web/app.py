from flask import Flask, Response, render_template, request, abort

app = Flask(__name__)


@app.route("/trivia.html",strict_slashes=False)
def trivia():
    return render_template("trivia.html")

@app.route("/trivia.css",strict_slashes=False)
def go_ducks():
	string = ""
	with open ("./static/styles/trivia.css","r",encoding='utf-8')as source:
		content = source.read()
	return render_template("template.html", content=content)


@app.errorhandler(404)
def error_404(e):

	return render_template("404.html")

@app.errorhandler(403)
def error_403(e):

	return render_template("403.html")

@app.route("/<path:path>",strict_slashes=False)
def new_(path):
	if ("//" in path) or (".." in path) or ("~" in path):
		return error_403("error")
	else:
		return error_404("error")
"""
@app.route("/<word>//<second>")
def new_2(word,second):
	return error_403("error")

"""

"""
@app.route("/<word>",strict_slashes=False)
def new_(word):
	if ("//" in word) or (".." in word) or ("~" in word):
		abort(403, description="not found")
	
	else:
		abort(404, description="not found")
"""


if __name__ == "__main__":
    app.run(port=5000, debug=True,host='0.0.0.0')
